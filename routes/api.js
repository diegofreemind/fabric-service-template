const express = require('express');

const router = express.Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../config/swagger.json');
const controller = require('../controller/api-controller');

router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(swaggerDocument));

router.post('/invoke', (req, res, next) => {

    controller.invoke(req.body)
        .then((obj) => res.send(obj))
        .catch(next);
});

router.get('/query', (req, res, next) => {

    controller.query(req.query)
        .then((obj) => res.send(obj))
        .catch(next);
});

router.use((err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send(err.message);
});


module.exports = router;