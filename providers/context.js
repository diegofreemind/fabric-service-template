/**
 * Connect the application to hyperledger fabric
 * Gateway - act as an abstraction between application and network details
 * Wallet - holds a set of user identities
 *
 * Reference:
 * https://hyperledger-fabric.readthedocs.io/en/release-1.4/developapps/designelements.html
 */

const path = require('path');
const env = require('../config/env');
const request = require('request-promise');
const connectionProfile = require('../config/network');
const { FileSystemWallet, Gateway } = require('fabric-network');

// Create a new file system based wallet for managing identities.
const walletPath = path.join(process.cwd(), 'wallet');
const wallet = new FileSystemWallet(walletPath);
console.log(`Wallet path: ${walletPath}`);

async function getCertificates(user) {

    try {

        let params = {
            method: 'POST',
            uri: `${env.fabric_ca_host}/api/auth`,
            headers: {
                'User-Agent': 'identity-service'
            },
            body: { identity: user },
            json: true
        };

        let userIdentity = await request(params);
        await wallet.import(env.identity, userIdentity);

        return { identity: user, status: 'enrolled' };

    } catch (err) {

        console.log(err.stack);
        return new Error(err.message);
    }

}

//join all elements to connect into network
async function connect(profile, options) {

    try {

        const gateway = new Gateway();
        await gateway.connect(profile, options);

        return gateway;

    } catch (err) {

        return new Error(err.message);
    }
}

//network + contract target
async function context(gateway, contract, namespace) {

    const network = await gateway.getNetwork(env.channel);
    const default_contract = await network.getContract(contract);

    return default_contract;
}

//disconnect this gateway from network
function disconnect(gateway) {

    gateway.disconnect();
}

(module.exports = async () => {

    let connectionOptions = {
        wallet: wallet,
        identity: env.identity,
        discovery: { enabled: false, asLocalhost: true }
    };

    try {

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists(env.identity);

        if (!userExists) {
            await getCertificates(env.identity);
        }

        const gateway = await connect(connectionProfile, connectionOptions);

        const chaincontext = await context(gateway, env.chaincode, null);
        return chaincontext;

    } catch (err) {

        //throws an error if connection to network fails
        console.log(err.stack);
        throw new Error(err.message);
    }

})();