module.exports = async (context, params) => {

    try {

        let { operation, values } = params;
        let { id, type, model, spec, owner } = values;

        let state = await context.submitTransaction(operation, id, type, model, spec, owner);
        return state;

    } catch (err) {

        return new Error(err.message);
    }
}