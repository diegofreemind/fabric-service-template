module.exports = async (context, params) => {

    try {

        let { operation, id } = params;
        let state = await context.evaluateTransaction(operation, id);
        return state.toString();

    } catch (err) {

        return new Error(err.message);
    }
}