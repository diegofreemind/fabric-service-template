const context = require('../providers/context');
const queryProvider = require('../providers/query');
const invokeProvider = require('../providers/invoke')

exports.invoke = async (params) => {

    try {

        const contract = await context();
        let res = invokeProvider(contract, params);
        return res;

    } catch (err) {
        return new Error(err.message);
    }
}

exports.query = async (params) => {

    try {

        const contract = await context();
        let res = queryProvider(contract, params);
        return res;

    } catch (err) {
        return new Error(err.message);
    }
}